import os
import csv
import glob
import pymongo
import numpy as np
import collections

Stock = collections.namedtuple('Stock', field_names=['code', 'date', 'index_price', 'index_num',
                                                     'open', 'high', 'low', 'close', 'volume'])
DATA_TYPE_TIC = 'tic'
DATA_TYPE_MIN = 'min'
DATA_TYPE_DAY = 'day'

def get_id_dic(data_type='tic'):
    conn = pymongo.MongoClient('127.0.0.1', 27017, username='myadmin', password='mongo3825')
    db = conn['mydb']
    coll_id = db['code_id']
    result = coll_id.find()
    dic_tic_id = {}
    dic_min_id = {}

    for doc in result:
        if 'tic_id' in doc:
            dic_tic_id[doc['_id']] = doc['tic_id']
        if 'min_id' in doc:
            dic_min_id[doc['_id']] = doc['min_id']

    if data_type == 'tic':
        return dic_tic_id
    elif data_type == 'min':
        return dic_min_id
    elif data_type == 'all':
        return dic_tic_id, dic_min_id


def read_db(code_list, data_type=DATA_TYPE_MIN):
    stocks_list = []
    conn = pymongo.MongoClient('127.0.0.1', 27017, username='myadmin', password='mongo3825')
    db = conn['mydb']
    coll_tic = db['day_info']
    coll_min = db['day_min']
    coll_sub = db['stock_subinfo']
    tic_id, min_id = get_id_dic('all')
    print("Get data from db", len(code_list), 'stocks')
    for stock in code_list:
        if data_type==DATA_TYPE_MIN:
            id = min_id[stock['code']][stock['time'][:8]]
            result_min = coll_min.find_one({'_id': id})
            d = result_min['date']
            o = np.array(result_min['open'][-stock['idx']-1::-1], np.float32)
            h = np.array(result_min['high'][-stock['idx']-1::-1], np.float32)
            l = np.array(result_min['low'][-stock['idx']-1::-1], np.float32)
            c = np.array(result_min['close'][-stock['idx']-1::-1], np.float32)
            v = np.array(result_min['amount'][-stock['idx']-1::-1], np.float32)
            i = c[0]
            stocks_list.append(Stock(code=stock['code'], date=d, index_num=stock['idx'],
                                     index_price=i, open=o, high=h, low=l, close=c, volume=v))

    return stocks_list

def read_list_csv(file_name, sep=','):
    print("Reading ", file_name)
    with open(file_name, 'rt', encoding='utf-8') as fd:
        reader = csv.reader(fd, delimiter=sep)
        h = next(reader)
        code_list = []
        count_out = 0

        prev_vals = None
        for row in reader:
            code_list.append({'code':str(row[0]), 'time':str(row[1]), 'idx':int(float(row[2]))})
            count_out += 1
    print("Read done, got {} rows".format(count_out))
    return code_list




def prices_to_relative(prices):
    """
    Convert prices to relative in respect to open price
    :param ochl: tuple with open, close, high, low
    :return: tuple with open, rel_close, rel_high, rel_low
    """
    assert isinstance(prices, Stock)
    rh = (prices.high - prices.open) / prices.open
    rl = (prices.low - prices.open) / prices.open
    rc = (prices.close - prices.open) / prices.open
    return Stock(open=prices.open, high=rh, low=rl, close=rc, volume=prices.volume)


def load_relative(csv_file):
    return prices_to_relative(read_db(csv_file))


def price_files(dir_name):
    result = []
    for path in glob.glob(os.path.join(dir_name, "*.csv")):
        result.append(path)
    return result


def load_year_data(year, basedir='data'):
    y = str(year)[-2:]
    result = {}
    for path in glob.glob(os.path.join(basedir, "*_%s*.csv" % y)):
        result[path] = load_relative(path)
    return result




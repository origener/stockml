from torch.utils.data import Dataset, DataLoader
import pandas as pd


class StockDataset(Dataset):
    def __init__(self, csv_file, before_day, transform=None):
        self.csv_list = pd.read_csv(csv_file)
        self.before_day = before_day
        self.transform = transform

    def __len__(self):
        return len(self.csv_list)

    def __getitem__(self, item):
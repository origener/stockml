import gym
import gym.spaces
from gym.utils import seeding
from gym.envs.registration import EnvSpec
import enum
import numpy as np

from . import StockData

DEFAULT_BARS_COUNT = 10
DEFAULT_COMMISSION_PERC = 0.3
DEFAULT_TRADE_PERC = 0.1


class Actions(enum.Enum):

    Skip = 0
   # Buy = 1
    Sell = 1


def relate_price(stock, index_price):
    assert isinstance(stock, StockData.Stock)
    o = (stock.open - index_price) / index_price
    h = (stock.high - index_price) / index_price
    l = (stock.low - index_price) / index_price
    c = (stock.close - index_price) / index_price
    v = stock.volume
    return StockData.Stock(code=stock.code, date=stock.date, index_price=index_price,
                           index_num=stock.index_num, open=o, high=h, low=l, close=c, volume=v)


class State:
    def __init__(self, bars_count, commission_per, trade_per, reward_on_close = True,
                 volumes=True, reset_on_close=True):
        assert isinstance(bars_count, int)
        assert bars_count > 0
        assert isinstance(commission_per, float)
        assert commission_per >= 0.0

        self.bars_count = bars_count
        self.commission_per = commission_per
        self.reward_on_close = reward_on_close
        self.reset_on_close = reset_on_close
        self.volumes = volumes
        self.trade_per = trade_per
        self.stock_bars_len = 1
        self.index_price = 0
        self.balance_weight = 0
        self.balance_mean_relate_price = 0.0
        self._prices = 0
        self._offset = 0
        self._position = True

        #debug
        self.valid_step_bar = 0

    def reset(self, stock):
        assert isinstance(stock, StockData.Stock)
        self.stock_bars_len = len(stock.open)
        self.balance_weight = 0
        self.balance_mean_relate_price = 0.0
        self.index_price = stock.index_price
        self._prices = relate_price(stock, self.index_price)
        self._offset = self.bars_count - 1
        self._position = True
    @property
    def shape(self):
        # [o, h, l, c, v] * bars + timegap (offset / stock's all bars length)
        if self.volumes:
            return 5 * self.bars_count + 1,
        else:
            return 4 * self.bars_count + 1,

    def encode(self):
        """
        Convert current state into numpy array.
        """
        res = np.ndarray(shape=self.shape, dtype=np.float32)
        shift = 0
        for bar_idx in range(-self.bars_count+1, 1):
            ofs = self._offset + bar_idx
            res[shift] = self._prices.open[ofs]
            shift += 1
            res[shift] = self._prices.high[ofs]
            shift += 1
            res[shift] = self._prices.low[ofs]
            shift += 1
            res[shift] = self._prices.close[ofs]
            shift += 1
            if self.volumes:
                res[shift] = self._prices.volume[ofs]
                shift += 1
        res[shift] = float(self._offset / self.stock_bars_len)
        #res[shift] = float(self.balance_weight)
        #shift += 1
        #res[shift] = float(self.balance_mean_relate_price)
        print(res)
        return res

    def _cur_close(self):
        """
        Calculate real close price for the current bar
        """
        rel_close = self._prices.close[self._offset]
        return self.index_price * (1.0 + rel_close)

    def step(self, action):
        """
        Perform one step in our price, adjust offset, check for the end of prices
        and handle position change
        :param action:
        :return: reward, done
        """
        """
        assert isinstance(action, Actions)
        reward = 0.0
        done = False
        prev_close = self._prices.close[self._offset]
        if action == Actions.Buy and not self.balance_weight == 100:
            self.balance_weight += int(self.trade_per * 100)
            self.balance_mean_relate_price += (prev_close - self.balance_mean_relate_price) \
                                              * ((self.trade_per*100) / self.balance_weight)
            #print(self.balance_mean_relate_price, self.balance_weight, prev_close, self.trade_per)
        elif action == Actions.Sell and not self.balance_weight == 0:
            reward -= self.commission_per * self.trade_per
            self.balance_weight -= int(self.trade_per * 100)
            if self.balance_weight == 0:
                done |= self.reset_on_close
                self.balance_mean_relate_price = 0.0
            if self.reward_on_close:
                reward += 100.0 * (prev_close - self.balance_mean_relate_price) * self.trade_per

        self._offset += 1
        close = self._prices.close[self._offset]
        done |= self._offset >= self._prices.close.shape[0]-1

        if self.balance_weight and not self.reward_on_close:
            reward += (prev_close - close) * self.balance_weight
        if done and self.balance_weight:
            reward -= self.commission_per * self.balance_weight
            reward += self.balance_weight * (close - self.balance_mean_relate_price)

        return reward, done
        """
        assert isinstance(action, Actions)
        reward = 0.0
        done = False
        prev_close = self._prices.close[self._offset]
        if action == Actions.Sell and self._position:
            reward -= self.commission_per
            if self.reward_on_close:
                reward = 100.0 * prev_close
            done |= self.reset_on_close
            self._position = False

        self._offset += 1
        close = self._prices.close[self._offset]
        done |= self._offset >= self._prices.close.shape[0] - 1

        if not self.reward_on_close:
            reward += 100.0 * (close - prev_close)
        if done and not self._position:
            reward += 100.0 * close

        return reward, done





class State1D(State):
    """
    State with shape suitable for 1D convolution
    """
    @property
    def shape(self):
        if self.volumes:
            return (6, self.bars_count)
        else:
            return (5, self.bars_count)

    def encode(self):
        res = np.zeros(shape=self.shape, dtype=np.float32)
        start = self._offset-(self.bars_count-1)
        stop = self._offset+1
        res[0] = self._prices.open[start:stop]
        res[1] = self._prices.high[start:stop]
        res[2] = self._prices.low[start:stop]
        res[3] = self._prices.close[start:stop]
        if self.volumes:
            res[4] = self._prices.volume[start:stop]
            dst = 5
        else:
            dst = 4
        res[dst] = list(map(lambda x: (self._offset + x) / self.stock_bars_len, range(-self.bars_count+1, 1)))
        return res


class StocksEnv(gym.Env):
    metadata = {'render.modes': ['human']}
    spec = EnvSpec("StocksEnv-v0")

    def __init__(self, stock_list, bars_count=DEFAULT_BARS_COUNT,
                 commission=DEFAULT_COMMISSION_PERC, trade_per=DEFAULT_TRADE_PERC,
                 reset_on_close=True, state_1d=False,
                 shuffle_list=True, reward_on_close=True,
                 volumes=True):
        assert isinstance(stock_list, list)
        self._stock_list = stock_list
        self.shuffle_list = shuffle_list
        self.bars_count = bars_count
        self.valid_offset = bars_count
        if state_1d:
            self._state = State1D(
                bars_count, commission, trade_per=trade_per, reset_on_close=reset_on_close,
                reward_on_close=reward_on_close, volumes=volumes)
        else:
            self._state = State(
                bars_count, commission, trade_per=trade_per, reset_on_close=reset_on_close,
                reward_on_close=reward_on_close, volumes=volumes)
        self.action_space = gym.spaces.Discrete(n=len(Actions))
        self.observation_space = gym.spaces.Box(
            low=-np.inf, high=np.inf,
            shape=self._state.shape, dtype=np.float32)
        self.seed()
        if shuffle_list:
            self.np_random.shuffle(self._stock_list)
        self._iter_stock_list = iter(self._stock_list)

    def reset(self):
        try:
            self._stock = next(self._iter_stock_list)
        except StopIteration:
            if self.shuffle_list:
                self.np_random.shuffle(self._stock_list)
            self._iter_stock_list = iter(self._stock_list)
            self._stock = next(self._iter_stock_list)
        #if self._stock == self._stock_list[-1]:
        #    if self.shuffle_list:
        #        self.np_random.shuffle(self._stock_list)
        #    self._iter_stock_list = iter(self._stock_list)
        self._instrument = self._stock.code
        self._state.reset(self._stock)
        return self._state.encode()

    def step(self, action_idx):
        action = Actions(action_idx)
        reward, done = self._state.step(action)
        obs = self._state.encode()
        info = {
            "instrument": self._instrument,
            "index_price" : self._stock.index_price,
            "index_num" : self._stock.index_num
        }
        return obs, reward, done, info

    def render(self, mode='human', close=False):
        pass

    def close(self):
        pass

    def seed(self, seed=None):
        self.np_random, seed1 = seeding.np_random(seed)
        seed2 = seeding.hash_seed(seed1 + 1) % 2 ** 31
        return [seed1, seed2]


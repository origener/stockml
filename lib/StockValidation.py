import numpy as np

import torch

from lib import StockEnviron

METRICS = (
    'episode_reward',
    'episode_steps',
    'order_profits',
)


def validation_run(env, net, episodes=100, device="cpu", epsilon=0.02, comission=0.3):
    stats = { metric: [] for metric in METRICS }

    for episode in range(episodes):
        obs = env.reset()
        position = env._stock.index_price
        total_reward = 0.0
        episode_steps = 0
        trade_per = 0.1
        balance_mean_price = 0
        balance_weight = 0

        while True:
            obs_v = torch.tensor([obs]).to(device)
            out_v = net(obs_v)

            action_idx = out_v.max(dim=1)[1].item()
            if np.random.random() < epsilon:
                action_idx = env.action_space.sample()
            action = StockEnviron.Actions(action_idx)
            close_price = env._state._cur_close()
            '''
            if action == StockEnviron.Actions.Buy and not balance_weight == 100:
                balance_weight += int(trade_per*100)
                balance_mean_price += (close_price - balance_mean_price) * ((trade_per*100) / balance_weight)
            elif action == StockEnviron.Actions.Sell and not balance_weight == 0:
                profit = (close_price - balance_mean_price) / balance_mean_price * trade_per
                profit = 100.0 * profit - comission * trade_per
                stats['order_profits'].append(profit)
                balance_weight -= int(trade_per*100)
            
            obs, reward, done, _ = env.step(action_idx)
            total_reward += reward
            episode_steps += 1
            if done:
                if balance_weight:
                    close_price = env._state._cur_close()
                    profit = (close_price - balance_mean_price) / balance_mean_price * balance_weight
                    profit = profit - (comission * balance_weight/100)
                    stats['order_profits'].append(profit)
                break
            '''

            if action == StockEnviron.Actions.Sell and position is not None:
                profit = close_price - position - (close_price + position) * comission / 100
                profit = 100.0 * profit / position
                stats['order_profits'].append(profit)
                position = None

            obs, reward, done, _ = env.step(action_idx)
            total_reward += reward
            episode_steps += 1

            if done:
                if position is not None:
                    close_price = env._state._cur_close()
                    profit = close_price - position - (close_price + position) * comission / 100
                    profit = 100.0 * profit / position
                    stats['order_profits'].append(profit)
                break

        stats['episode_reward'].append(total_reward)
        stats['episode_steps'].append(episode_steps)

    return { key: np.mean(vals) for key, vals in stats.items() }

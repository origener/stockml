import ptan
import pathlib
import argparse
import gym.wrappers
import numpy as np

import torch
import torch.optim as optim

from ignite.engine import Engine
from ignite.contrib.handlers import tensorboard_logger as tb_logger

from lib import StockEnviron, StockData, models, common, StockValidation

SAVES_DIR = pathlib.Path("saves")
STOCKS = "data/20210108.csv"
VAL_STOCKS = "data/stock_202006_202101.csv"

BATCH_SIZE = 32
BARS_COUNT = 10

EPS_START = 1.0
EPS_FINAL = 0.1
EPS_STEPS = 1000000

GAMMA = 0.99

REPLAY_SIZE = 100000
REPLAY_INITIAL = 10000
REWARD_STEPS = 2
LEARNING_RATE = 0.0001
STATES_TO_EVALUATE = 1000

def convert_real_price(prices):
    index_price = prices[3]

def valid_decode(env):
    reset_encode = env.reset()
    index_price = env._stock.index_price
    decode = reset_encode[:4]
    decode = index_price * (1+decode)
    print(decode)
    num_count = 1
    while True:
        next_decode , r, d, i = env.step(0)
        if d:
            decode = next_decode[-6:-2]
            decode = index_price * (1 + decode)
            print(decode, i)
            print(f'{num_count} Done -------> reset')
            num_count += 1
            reset_encode = env.reset()
            index_price = env._stock.index_price
            decode = reset_encode[:4]
            decode = index_price * (1 + decode)
            print(decode)



if __name__ == "__main__":
    device = torch.device("cuda")
    csv_list = StockData.read_list_csv(STOCKS)
    csv_list_val = StockData.read_list_csv(VAL_STOCKS)
    stock_list = StockData.read_db(csv_list)
    env = StockEnviron.StocksEnv(stock_list, bars_count=BARS_COUNT)
    print(env.reset())

